@file:Suppress("unused")

package com.lazygeniouz.appopenads

import android.app.Application
import com.applovin.sdk.AppLovinMediationProvider
import com.applovin.sdk.AppLovinSdk
import com.lazygeniouz.aoa.configs.Configs
import com.lazygeniouz.aoa.extensions.getAppOpenAdManager
import com.lazygeniouz.aoa.idelay.InitialDelay
import com.lazygeniouz.aoa.listener.AppOpenAdListener
import com.lazygeniouz.appopenads.activities.SplashActivity


/**
 * Sample App's Main Application
 * Kotlin Version, registered in the Manifest.
 * */
class AppKt : Application() {

    override fun onCreate() {
        super.onCreate()

        //AppLovinSdk.getInstance(this).showMediationDebugger();
        AppLovinSdk.getInstance(this).mediationProvider = AppLovinMediationProvider.MAX
        AppLovinSdk.initializeSdk(this) { }
        getAppOpenAdManager(
            Configs(
                InitialDelay.NONE,
                "ed44cec2c939b34f",/*Replace Your Unit Id*/
                showInActivities = arrayListOf(SplashActivity::class.java),
            )
        ).setAppOpenAdListener(object : AppOpenAdListener() {
            override fun onAdShown() = println("AppOpenAdListener#onAdShown")
            override fun onAdLoaded() = println("AppOpenAdListener#onAdLoaded")
            override fun onAdWillShow() = println("AppOpenAdListener#onAdWillShow")
        }).loadAppOpenAd()
    }
}