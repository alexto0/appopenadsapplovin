package com.lazygeniouz.appopenads;

import android.app.Application;

import com.applovin.sdk.AppLovinMediationProvider;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.lazygeniouz.aoa.AppOpenAdManager;

/**
 * Sample App's Main Application
 * Java Version, not registered in the manifest.
 */
@SuppressWarnings({"unused", "RedundantSuppression"})
public class AppJv extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        AppLovinSdk.getInstance(this).setMediationProvider(AppLovinMediationProvider.MAX);
        AppLovinSdk.initializeSdk( this, configuration -> {
        });

        AppOpenAdManager manager = AppOpenAdManager.get(this);
        manager.loadAppOpenAd();
    }
}
